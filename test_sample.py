# content of test_sample.py
import pytest
import requests
def func(x):
    return x + 3
    
@pytest.mark.xfail
def test_answer():
    assert func(3) == 5


def hello():
  print('hello world')

if __name__=="__main__":
  hello()
